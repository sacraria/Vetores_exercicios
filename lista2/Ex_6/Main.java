import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        System.out.printf("Digite: ");
        String inputStr = keyboard.next();
        keyboard.close();
        char[] vetor = inputStr.toCharArray();
        int i;
        int j = 0;

        for (i = 3; i >= 0; i--) {
            if (vetor[i] != vetor[j]) {
                System.out.println("Não!");
                System.exit(1);
            }
            j++;
        }

        System.out.println("Sim!");
    }
}
