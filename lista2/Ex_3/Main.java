import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        float[] nota = new float[4];
        float notaMedia;
        boolean continuar = true;

        while (continuar == true) {
            for (int i = 0; i < 4; i++) {
                System.out.printf("Digite a %d⁰ nota do aluno: ", i + 1);
                nota[i] = keyboard.nextFloat();
                if (nota[i] > 10 || nota[i] < -1) {
                    System.out.println("nota inválida");
                    i--;
                }
            }

            notaMedia = 0;

            for (int i = 0; i < 4; i++) {
                if (nota[i] == -1) {
                    notaMedia += 1;
                }
            }

            if (notaMedia == 4) {
                continuar = false;
            } else {
                for (int i = 0; i < 4; i++) {
                    if (nota[i] == -1) {
                        nota[i] = 0;
                    }
                }

                notaMedia = 0;

                for (int i = 0; i < 4; i++) {
                    notaMedia += nota[i];
                    System.out.println(nota[i]);
                }
                notaMedia /= 4;
                System.out.println("A média do aluno é " + notaMedia);
            }
        }
        keyboard.close();
    }
}
