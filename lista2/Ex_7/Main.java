import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        double[][] vetor = new double[5][20];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.printf("Dentro do vetor %d, digite o número da posição %d: ", i + 1, j);
                vetor[i][j] = keyboard.nextDouble();
            }
        }

        for (int i = 0; i < 20; i++) {

            if (vetor[0][i] > vetor[1][i]) {
                vetor[2][i] = vetor[0][i] - vetor[1][i];
            } else {
                vetor[2][i] = vetor[1][i] - vetor[0][i];
            }

            vetor[3][i] = vetor[0][i] + vetor[1][i];
            vetor[4][i] = vetor[0][i] * vetor[1][i];
        }
    }
}
