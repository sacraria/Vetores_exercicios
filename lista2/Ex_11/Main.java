import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        float[] altura = new float[10];
        float media = 0;
        for (int i = 0; i < 10; i++) {
            System.out.printf("Digite a altura do %d⁰ jogador: ", i + 1);
            try {
                altura[i] = keyboard.nextFloat();
                media += altura[i];
            } catch (InputMismatchException e) {
                System.out.println("Digite um número válido!");
                i--;
            }
        }
        keyboard.close();
        media /= 10;
        for (int i = 0; i < 10; i++) {
            if (altura[i] > media) {
                System.out.printf("O %d⁰ Jogador tem a altura maior que a média\n", i + 1);
            }
        }
    }
}
