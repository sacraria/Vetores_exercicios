package Ex_18;

public class Main {
    public static void main(String args[]) {
        int argsLength = args.length;
        if (argsLength == 0) {
            System.out.printf("Insira pelo menos um número como argumento!\n");
        }
        double[] vetor = new double[argsLength];
        for (int i = 0; i < argsLength; i++) {
            try {
                vetor[i] = Double.parseDouble(args[i]);
            } catch (NumberFormatException e) {
                System.out.printf("O número na posição %d é inválido!\n", i + 1);
                System.exit(1);
            }
            if (vetor[i] % 2 == 0) {
                System.out.printf("Na posição %d, a raiz de %f é %f\n", i + 1, vetor[i], Math.sqrt(vetor[i]));
            } else {
                System.out.printf("Na posição %d, a potência de %f é %f\n", i + 1, vetor[i], Math.pow(vetor[i], 2));
            }
        }
    }
}
