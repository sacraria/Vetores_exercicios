import java.util.Random;

public class Main {
    public static void main(String args[]) {
        double[] vetor = new double[10];
        Random rand = new Random();
        double S = 0;
        double maior = 0, menor = 0;
        for (int i = 0; i < 10; i++) {
            vetor[i] = rand.nextDouble(768);
            S += vetor[i];
        }

        System.out.println("soma: " + S);

        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                maior = vetor[i];
                menor = vetor[i];
            } else {
                if (vetor[i] > maior) {
                    maior = vetor[i];
                }
                if (vetor[i] < menor) {
                    menor = vetor[i];
                }
            }
        }
        System.out.println("menor: " + menor);
        System.out.println("maior: " + maior);

    }
}
