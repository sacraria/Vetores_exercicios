import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        System.out.printf("Digite a quantia de números em cada vetor: ");
        int quantiaVetor = keyboard.nextInt();
        double[][] vetor = new double[3][quantiaVetor];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < quantiaVetor; j++) {
                System.out.printf("Dentro do %d⁰ vetor, digite o número para a posição %d: ", i + 1, j);
                vetor[i][j] = keyboard.nextDouble();
            }
        }
        keyboard.close();

        int j = 0;

        for (int i = 0; i < quantiaVetor; i++) {
            if (vetor[0][i] == vetor[1][i]) {
                vetor[2][j] = vetor[0][i];
                j++;
            }
        }

    }
}
