import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Random;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();
        int X = 0;
        int i;
        int[] vetor = new int[20];
        for (i = 0; i < 20; i++) {
            vetor[i] = rand.nextInt(100);
        }
        System.out.printf("Digite o valor entre 0 e 100: ");
        try {
            X = keyboard.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Erro! o número não é um inteiro");
            keyboard.next();
            System.exit(1);
        }

        for (i = 0; i < 20; i++) {
            if (X == vetor[i]) {
                System.out.println("O número foi achado na posição " + i);
                i = 20;
            }
        }
        if (i != 21) {
            System.out.println("O número não foi encontrado");
        }
    }
}
