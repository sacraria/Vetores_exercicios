import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();
        int[] vetor = new int[5];
        int procura;
        int vezesAchado = 0;

        for (int i = 0; i < 5; i++) {
            vetor[i] = rand.nextInt(20);
        }

        System.out.printf("Digite o número que deseja procurar: ");
        procura = keyboard.nextInt();
        keyboard.close();

        for (int i = 0; i < 5; i++) {
            if (vetor[i] == procura) {
                System.out.printf("O número é achado na posição %d\n", i);
                vezesAchado += 1;
            }
        }
        System.out.printf("O número foi achado %d vezes\n", vezesAchado);

    }
}
