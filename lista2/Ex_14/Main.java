public class Main {
    public static void main(String args[]) {

        Double[] vetor2 = new Double[100];
        // Para dar 100 argumentos facilmente use {número1..número2}
        // ex: java Main {-50..49}
        if (args.length != 100) {
            System.out.println("100 argumentos por favor");
            System.exit(1);
        }
		int j = 0;
        for (int i = 0; i < 100; i++) {
            try {
                vetor2[j] = Double.parseDouble(args[i]);
            } catch (NumberFormatException e) {
                System.out.printf("o argumento %s não é válido!\n", args[i]);
                System.exit(1);
            }
            if (vetor2[j] <= 0) {
                vetor2[j] = null;
            } else {
                System.out.printf("%f\n", vetor2[j]);
				j++;
            }
        }
    }
}