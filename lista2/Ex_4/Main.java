import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        double[] X = new double[20];

        for (int i = 0; i < 20; i++) {
            System.out.printf("Digite: ");
            X[i] = keyboard.nextDouble();
        }
        keyboard.close();

        double[] Y = new double[20];
        double[] Z = new double[20];

        int y = 0;
        int z = 0;

        for (int i = 0; i < 20; i++) {
            if (X[i] % 2 == 0) {
                Y[y] = X[i];
                System.out.println("posição " + i + " (" + y + " no vetor Y) é par");
                y++;
            } else {
                Z[z] = X[i];
                System.out.println("posição " + i + " (" + z + " no vetor Z) é impar");
                z++;
            }
        }
    }
}
