package Ex_17;

public class Main {
    public static void main(String args[]) {
        if (args.length != 20) {
            System.out.printf("Número de argumentos inválido, digite 20 argumentos\n");
            System.exit(1);
        }

        double[] vetor = new double[20];
        int i = 0;
        int j = 19;

        while (i <= 9) {
            try {
                vetor[i] = Double.parseDouble(args[j]);
                vetor[j] = Double.parseDouble(args[i]);
            } catch (NumberFormatException e) {
                System.out.printf("O %s⁰ argumento ou o %s⁰ argumento é inválido\n", i + 1, j + 1);
                System.exit(1);
            }

            i++;
            j--;

        }

        for (i = 0; i < 20; i++) {
            System.out.printf("%f\n", vetor[i]);
        }
    }
}
