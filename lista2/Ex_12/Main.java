import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        double[] vetor = new double[10];
        for (int i = 0; i < 10; i++) {
            try {
                System.out.printf("Digite o valor para o índice %d: ", i);
                vetor[i] = keyboard.nextDouble();
                vetor[i] *= i;
            } catch (InputMismatchException e) {
                i--;
                System.out.println("Digite um número válido!");
            }
        }

        for (int i = 0; i < 10; i++) {
            System.out.printf("Dentro do índice %d -> %f\n", i, vetor[i]);
        }

    }
}
