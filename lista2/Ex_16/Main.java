package Ex_16;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        int[] idade = new int[20];
        double[] altura = new double[20];
        double[] peso = new double[20];
        double[] imc = new double[20];
        double media = 0;

        for (int i = 0; i < 20; i++) {
            try {
                System.out.printf("Digite a idade da pessoa %d: ", i + 1);
                idade[i] = keyboard.nextInt();
                System.out.printf("Digite a altura da pessoa %d: ", i + 1);
                altura[i] = keyboard.nextDouble();
                System.out.printf("Digite o peso da pessoa %d em Kg: ", i + 1);
                peso[i] = keyboard.nextDouble();
            } catch (InputMismatchException e) {
                i--;
                System.out.printf("Algum valor inserido foi inválido\n");
                keyboard.next();
            }
        }

        keyboard.close();

        for (int i = 0; i < 20; i++) {
            imc[i] = peso[i] * (altura[i] * altura[i]);
            media += imc[i];
        }

        media /= 20;

        for (int i = 0; i < 20; i++) {
            if (imc[i] > media) {
                System.out.printf("A %d⁰ pessoa está com o peso maior que a média\n", i + 1);
            } else if (imc[i] == media) {
                System.out.printf("A %d⁰ pessoa está com o peso exatamente na média\n", i + 1);
            } else {
                System.out.printf("A %d⁰ pessoa está com o peso menor que média\n", i + 1);
            }
        }
    }
}
