package Ex_15;

public class Main {
    public static void main(String args[]) {
        double[] vetor = new double[50];
        if (args.length != 50) {
            System.out.printf("insira 50 argumentos!\n");
            System.exit(1);
        }

        for (int i = 0; i < 50; i++) {
            try {
                vetor[i] = Double.parseDouble(args[i]);
            } catch (NumberFormatException e) {
                System.out.printf("%s não é um número válido!\n", args[i]);
                System.exit(1);
            }

            if (vetor[i] % 2 == 0) {
                vetor[i] *= 1.02;
            } else {
                vetor[i] *= 1.05;
            }

            System.out.printf("%f\n", vetor[i]);

        }
    }
}
