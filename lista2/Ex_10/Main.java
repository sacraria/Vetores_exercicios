import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        int[] vetor = new int[10];
		int[] vetor2 = new int[10];
        int multiuso;
        for (int i = 0; i < 10; i++) {
            System.out.printf("Digite o número (positivo) para a posição %d: ", i);
            vetor[i] = keyboard.nextInt();
        }
		int j = 0;
        for (int i = 0; i < 10; i++) {
            multiuso = vetor[i];
            vetor[i] /= 2;
            while (vetor[i] > 1) {
                if (multiuso % vetor[i] == 0) {
                    System.out.printf("O número %d não é primo!\n", multiuso);
                    multiuso = -1;
                    vetor[i] = 0;
                }
                vetor[i]--;
            }
            if (multiuso != -1) {
                System.out.printf("O número %d é primo\n", multiuso);
				vetor2[j] = multiuso;
				j++;
            }
        }

		for (int i = 0; i < 10; i++) {
			System.out.println(vetor2[i]);
		}
    }
}
