import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        double[] A = new double[5];
        double[] B = new double[5];
        for (int i = 0; i < 5; i++) {
            System.out.printf("Digite o %d⁰ número: ", i + 1);
            A[i] = keyboard.nextDouble();
        }
        keyboard.close();
        for (int i = 0; i < 5; i++) {
            B[i] = A[i] * 2;
            System.out.println(B[i]);
        }
    }
}
