import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        double[] vetor = new double[3];
        for (int i = 0; i < 3; i++) {
            System.out.printf("Digite o número do %d⁰ vetor: ", i + 1);
            vetor[i] = keyboard.nextDouble();
        }
        keyboard.close();
        System.out.printf("O produto dos três números é: %f ", vetor[0] * vetor[1] * vetor[2]);
    }
}
