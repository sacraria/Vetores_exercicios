import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        float[] pessoa = new float[5];
        float media = 0;
        for (int i = 0; i < 5; i++) {
            System.out.printf("Digite o peso da %d⁰ pessoa: ", i + 1);
            pessoa[i] = keyboard.nextFloat();
        }
        keyboard.close();
        for (int i = 0; i < 5; i++) {
            media += pessoa[i];
        }
        media /= 5;
        System.out.println("A média de pessoa entre as pessoa é " + media);
    }
}
