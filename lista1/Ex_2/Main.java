import java.util.Scanner;

public class Main {
	public static void main (String args[]) {
		Scanner keyboard = new Scanner(System.in);
		double[] vetor = new double[5];
		System.out.printf("Digite o número: ");
		vetor[0] = keyboard.nextDouble();
		keyboard.close();
		for (int i = 1; i < 5; i++) {
			vetor[i] = vetor[i - 1] + 2;
			System.out.println(vetor[i]);
		}
	}
}
